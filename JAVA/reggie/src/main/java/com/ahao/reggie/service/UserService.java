package com.ahao.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ahao.reggie.entity.Employee;
import com.ahao.reggie.entity.User;

public interface UserService extends IService<User> {
}
