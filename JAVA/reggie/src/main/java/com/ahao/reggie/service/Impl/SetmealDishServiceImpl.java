package com.ahao.reggie.service.Impl;

import com.ahao.reggie.dto.SetmealDto;
import com.ahao.reggie.entity.SetmealDish;
import com.ahao.reggie.mapper.SetmealDishMapper;
import com.ahao.reggie.service.SetmealDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {

}
