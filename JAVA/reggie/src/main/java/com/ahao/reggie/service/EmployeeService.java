package com.ahao.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ahao.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee>{
}
